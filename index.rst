Công ty TNHH Sản xuất Thương mại Mộc Mây - Tinh dầu Mộc Mây (https://tinhdaumocmay.com/) là xưởng sản xuất tinh dầu hàng đầu Việt Nam. Công ty đã cho ra đời nhiều sản phẩm tinh dầu nguyên chất 100% hữu cơ sử dụng lò chưng cất hiện đại, chế tạo bằng inox cao cấp dùng trong y dược để bảo đảm chất lượng và giữ mùi hương tự nhiên đạt chuẩn GMP. 

Các chứng nhận Tinh dầu Mộc Mây đã đạt được

Các dòng sản phẩm tiêu biểu của Tinh dầu Mộc Mây như: tinh dầu bạc hà, tinh dầu bạch đàn chanh, tinh dầu bưởi chùm (bưởi hồng), tinh dầu bưởi da xanh, tinh dầu cà phê, tinh dầu cam, tinh dầu chanh thái (chanh sần), tinh dầu cỏ xạ hương, tinh dầu dứa, tinh dầu đinh hương, tinh dầu gỗ đàn hương, tinh dầu gỗ hồng, tinh dầu gỗ thông, tinh dầu gỗ tuyết tùng, tinh dầu gừng, tinh dầu hoa hồng, tinh dầu hoa lài (nhài), tinh dầu hoa ngũ sắc, tinh dầu hoa phong lữ, tinh dầu hoa sen trắng, tinh dầu hoa tulip, tinh dầu húng quế, tinh dầu hương nhu, tinh dầu hương thảo, tinh dầu khuynh diệp, tinh dầu ngọc lan tây, tinh dầu oải hương (lavender) pháp, tinh dầu quế, tinh dầu quýt, tinh dầu sả chanh, tinh dầu sả chanh organic, tinh dầu sả java, tinh dầu táo, tinh dầu trà xanh, tinh dầu tràm gió loại 1, tinh dầu tràm gió loại cao cấp, tinh dầu trầm hương, tinh dầu tràm trà... tinh dầu nguyên chất thiên nhiên 100%, an toàn cho người sử dụng tại TP Hồ Chí Minh, Hà Nội và toàn quốc.

Xem thêm: https://vi.wikipedia.org/wiki/Tinh_d%E1%BA%A7u

Tinh dầu bạc hà: https://tinhdaumocmay.com/san-pham/tinh-dau-bac-ha/

tinh dầu bạch đàn chanh: https://tinhdaumocmay.com/san-pham/tinh-dau-bach-dan-chanh/

tinh dầu bưởi chùm (bưởi hồng): https://tinhdaumocmay.com/san-pham/tinh-dau-buoi-chum-buoi-hong/

tinh dầu bưởi da xanh: https://tinhdaumocmay.com/san-pham/tinh-dau-buoi-da-xanh/

tinh dầu cà phê: https://tinhdaumocmay.com/san-pham/tinh-dau-ca-phe/

tinh dầu cam: https://tinhdaumocmay.com/san-pham/tinh-dau-cam/

tinh dầu chanh thái (chanh sần): https://tinhdaumocmay.com/san-pham/tinh-dau-chanh-thai-chanh-san/

tinh dầu cỏ xạ hương: https://tinhdaumocmay.com/san-pham/tinh-dau-co-xa-huong/

tinh dầu dứa: https://tinhdaumocmay.com/san-pham/tinh-dau-dua/

tinh dầu đinh hương: https://tinhdaumocmay.com/san-pham/tinh-dau-dinh-huong/

tinh dầu gỗ đàn hương: https://tinhdaumocmay.com/san-pham/tinh-dau-go-dan-huong/

tinh dầu gỗ hồng: https://tinhdaumocmay.com/san-pham/tinh-dau-go-hong/

tinh dầu gỗ thông: https://tinhdaumocmay.com/san-pham/tinh-dau-go-thong/

tinh dầu gỗ tuyết tùng: https://tinhdaumocmay.com/san-pham/tinh-dau-go-tuyet-tung/

tinh dầu gừng: https://tinhdaumocmay.com/san-pham/tinh-dau-gung/

tinh dầu hoa hồng: https://tinhdaumocmay.com/san-pham/tinh-dau-hoa-hong/

tinh dầu hoa lài (nhài): https://tinhdaumocmay.com/san-pham/tinh-dau-hoa-lai-nhai/

tinh dầu hoa ngũ sắc: https://tinhdaumocmay.com/san-pham/tinh-dau-hoa-ngu-sac/

tinh dầu hoa phong lữ: https://tinhdaumocmay.com/san-pham/tinh-dau-hoa-phong-lu/

tinh dầu hoa sen trắng: https://tinhdaumocmay.com/san-pham/tinh-dau-hoa-sen-trang/

tinh dầu hoa tulip: https://tinhdaumocmay.com/san-pham/tinh-dau-hoa-tulip/

tinh dầu húng quế: https://tinhdaumocmay.com/san-pham/tinh-dau-hung-que/

tinh dầu hương nhu: https://tinhdaumocmay.com/san-pham/tinh-dau-huong-nhu/

tinh dầu hương thảo: https://tinhdaumocmay.com/san-pham/tinh-dau-huong-thao/

tinh dầu khuynh diệp: https://tinhdaumocmay.com/san-pham/tinh-dau-khuynh-diep/

tinh dầu ngọc lan tây: https://tinhdaumocmay.com/san-pham/tinh-dau-ngoc-lan-tay/

tinh dầu oải hương (lavender) pháp: https://tinhdaumocmay.com/san-pham/tinh-dau-oai-huong-lavender-phap/

tinh dầu quế: https://tinhdaumocmay.com/san-pham/tinh-dau-que/

tinh dầu quýt: https://tinhdaumocmay.com/san-pham/tinh-dau-quyt/

tinh dầu sả chanh: https://tinhdaumocmay.com/san-pham/tinh-dau-sa-chanh/

tinh dầu sả chanh organic: https://tinhdaumocmay.com/san-pham/tinh-dau-sa-chanh-organic/

tinh dầu sả java: https://tinhdaumocmay.com/san-pham/tinh-dau-sa-java/

tinh dầu táo: https://tinhdaumocmay.com/san-pham/tinh-dau-tao/

tinh dầu trà xanh: https://tinhdaumocmay.com/san-pham/tinh-dau-tra-xanh/

tinh dầu tràm gió loại 1: https://tinhdaumocmay.com/san-pham/tinh-dau-tram-gio-loai-1/

tinh dầu tràm gió loại cao cấp: https://tinhdaumocmay.com/san-pham/tinh-dau-tram-gio-loai-cao-cap/

tinh dầu trầm hương: https://tinhdaumocmay.com/san-pham/tinh-dau-tram-huong/

tinh dầu tràm trà: https://tinhdaumocmay.com/san-pham/tinh-dau-tram-tra/

Tinh dầu Mộc Mây cung cấp đến quý khách hàng các sản phẩm tổng hợp khác như: tinh dầu đuổi chuột, tinh dầu ngủ ngon, tinh dầu dễ ngủ, tinh dầu hồi, tinh dầu xông hơi, tinh dầu thư giãn, 

Xem chi tiết:

tinh dầu đuổi chuột: https://tinhdaumocmay.com/tinh-dau-duoi-chuot/

tinh dầu dễ ngủ: https://tinhdaumocmay.com/tinh-dau-de-ngu/

tinh dầu hồi: https://tinhdaumocmay.com/tinh-dau-hoi/

tinh dầu xông hơi: https://tinhdaumocmay.com/tinh-dau-xong-hoi/

tinh dầu thư giãn: https://tinhdaumocmay.com/tinh-dau-thu-gian/

Các sản phẩm tinh dầu của Công ty được quản lý, kiểm soát chặt chẽ từ mẫu đất trồng, nguồn nước tưới, phân bón, nguồn nguyên liệu đầu vào, đến chế biến, đóng gói, lưu kho, xuất xưởng, vận chuyển và phân phối đến người tiêu dùng đều đạt tiêu chuẩn Organic hữu cơ. Nhờ đó, sản phẩm đã đạt tiêu chuẩn xuất khẩu của trung tâm giám định Vinacontrol, Quatest 3, Bộ y tế…

Giải thưởng Thương hiệu mạnh Đất Việt mà Tinh dầu Mộc Mây đạt được

Năm 2017, sản phẩm tinh dầu Mộc Mây được tôn vinh trong lễ trao giải thưởng doanh nghiệp nông nghiệp tiêu biểu; Top 100 thương hiệu vàng nông nghiệp VN 2019; Top 10 thương hiệu mạnh Đất Việt 2020 và một số giải thưởng khác…Hiện tại chúng tôi đang bán sỉ cho hầu hết các đại lý lớn nhỏ và nhiều Cty dược, mỹ phẩm trong và ngoài nước.

Công ty TNHH Sản Xuất Thương Mại Mộc Mây

Đại chỉ: Số 8 Số 8, Hiệp Bình Chánh, Thủ Đức, Thành phố Hồ Chí Minh, Việt Nam
Số điện thoại: 0907789009
Email: mocmayfarm@gmail.com
Website: https://tinhdaumocmay.com
Chỉ đường: https://goo.gl/maps/CVnrATypGF6dRaxz6

Theo dõi chúng tôi tại:
Facebook: https://www.facebook.com/tinhdaumocmay/
Twitter: https://twitter.com/tinhdaumocmay/
Youtube: https://www.youtube.com/channel/UCPnk9AHAI-A8_i2xco5QJ2A/
Linkedin: https://www.linkedin.com/in/tinhdaumocmay/
Instagram: https://www.instagram.com/tinhdaumocmay/
Behance: https://www.behance.net/tinhdaumocmay/
Pinterest: https://www.pinterest.com/tinhdaumocmay/
Tumblr: https://tinhdaumocmay.tumblr.com/
Google Earth: https://earth.google.com/earth/d/1_uOfb_S7DtLN0fdk9Jly-Y1NvulQeBBb?usp=sharing
Trang vàng: http://trangvangtructuyen.vn/tinh-dau-moc-may-cong-ty-tnhh-san-xuat-thuong-mai-moc-may.html
